﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Fake_news
{
    public partial class ListItem : UserControl
    {
        public ListItem()
        {
            InitializeComponent();
        }

        private string title;
        private string description;
        private string source;
        private DateTime published;
        private Image image;
        private string url;

        [Category("Costum Props")]
        public string Title {
            get => title;
            set
            {
                title = value;
                labelTitle.Text = value;
            }
        }
        [Category("Costum Props")]
        public string Description {
            get => description;
            set
            {
                description = value;
                labelDescription.Text = value;
            }

        }

        [Category("Costum Props")]
        public string Source {
            get => source;
            set
            {
                source = value;
                labelSource.Text = value;
            }
        }
        [Category("Costum Props")]
        public DateTime Published {
            get => published;
            set
            {
                published = value;
                labelPublished.Text = value.ToString();
            } }

        [Category("Costum Props")]
        public Image Image {
            get => image;
            set
            {
                image = value;
                pictureBoxImage.Image = value;
            } }

        [Category("Costum Props")]
        public string Url
        {
            get => url;
            set
            {
                url = value;
            }
        }

        private void pictureBoxImage_Click(object sender, EventArgs e)
        {
            Process.Start(Url);
        }
    }
}
