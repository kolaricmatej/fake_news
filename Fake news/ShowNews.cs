﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NewsAPI;
using NewsAPI.Constants;
using NewsAPI.Models;

namespace Fake_news
{
    public partial class ShowNews : Form
    {
        private NewsApiClient apiClient;
        private ArticlesResult articleResponse;
        private string[] category;
        public ShowNews()
        {
            InitializeComponent();
            
            try
            {
                apiClient = new NewsApiClient("cebd043869544620b7b47b8b03846a12");
            }
            catch (Exception e)
            {
                apiClient = new NewsApiClient("6b4bbc08acee4f449da6d26b46bd6c13");
            }
            string[] language = new string[] { "Select", "EN", "HR", "DE", "CH" };
            comboBoxLanguage.DataSource = language;
        }

        private void ShowNews_Load(object sender, EventArgs e)
        {
            
        }

        private string ChangeApi()
        {

            if (articleResponse.Status == Statuses.Error)
            {
                return "6b4bbc08acee4f449da6d26b46bd6c13";
            }
            else
            {
                return "cebd043869544620b7b47b8b03846a12";
            }
        }
        private void ArrangeCategory()
        {
            if (comboBoxLanguage.SelectedItem == "EN" || comboBoxLanguage.SelectedItem == "Select")
            {
                category = new string[] { "News", "Sport", "Health", "Technology" };
            }
            else if (comboBoxLanguage.SelectedItem == "HR")
            {
                category = new string[] { "Vijesti", "Sport", "Zdravlje", "Tehnologija" };
            }
            else if (comboBoxLanguage.SelectedItem == "DE")
            {
                category = new string[] { "Nachrichten", "Sport", "Gesundheit", "Technologie" };
            }
            else if (comboBoxLanguage.Text == "IT")
            {
                category = new string[] { "Notizie", "Sport", "Salute", "Tecnologia" };
            }
            comboBoxCategory.DataSource = category;
        }
        private void getDataFromNews()
        {
            flowLayoutPanel1.Controls.Clear();
            if (articleResponse != null)
            {
                ListItem[] list = new ListItem[articleResponse.Articles.Count];
                for (int i = 0; i < 1; i++)
                {
                    foreach (var article in articleResponse.Articles)
                    {
                        list[i] = new ListItem();
                        list[i].Title = article.Title;
                        list[i].Published = (DateTime)article.PublishedAt;
                        list[i].Source = article.Source.Name;
                        list[i].Description = article.Description;
                        list[i].Url = article.Url;
                        WebClient wc = new WebClient();
                        byte[] bytes = wc.DownloadData(article.UrlToImage);
                        MemoryStream ms = new MemoryStream(bytes);
                        list[i].Image = System.Drawing.Image.FromStream(ms);

                            flowLayoutPanel1.Controls.Add(list[i]);
                        
                    }
                }
            }
        }

        private int ShowYear()
        {
            if (DateTime.Now.Month == 1)
            {
                return DateTime.Now.Year - 1;
            }
            else
            {
                return DateTime.Now.Year;
            }
        }
        private int ShowMonth()
        {
            if (DateTime.Now.Day < 10)
            {
                return DateTime.Now.Month - 1;
            }
            else
            {
                return DateTime.Now.Month;
            }
        }
        private int ShowDay()
        {
            if (DateTime.Now.Day < 10)
            {
                return DateTime.DaysInMonth(ShowYear(), ShowMonth()) - (10 - DateTime.Now.Day);
            }
            return DateTime.Now.Day - 10;
        }



        private async void comboBoxLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {  
            string selected = (string)comboBoxLanguage.SelectedItem;
            ArrangeCategory();
            string category = (string)comboBoxCategory.SelectedItem;
            if (category == null)
            {
                category = "News";
            }
            if ( selected == "HR")
            {
                await ShowCroatianNews(category);
            }
            else if (selected == "Select" || selected == "EN")
            {
               await ShowEnglishNews(category);
            }
            else if (selected == "DE")
            {
                ShowGermanNews(category);
            }
        }

        private async Task ShowEnglishNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = comboBoxCategory.SelectedItem.ToString(),
                SortBy = SortBys.Popularity,
                Language = Languages.EN,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async Task ShowCroatianNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.HR,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async Task ShowGermanNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.DE,
                PageSize = 50,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }
        private async Task ShowItalianNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.IT,
                PageSize = 50,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async void comboBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = (string)comboBoxCategory.SelectedItem;

            if (comboBoxLanguage.Text == "HR" )
            {
                await ShowCroatianNews(selected);
            }
            else if (comboBoxLanguage.Text == "EN" || comboBoxLanguage.Text == "Select")
            {
                await ShowEnglishNews(selected);
            }
            else if (comboBoxLanguage.Text == "DE")
            {
                await ShowGermanNews(selected);
            }
            else if (comboBoxLanguage.Text == "IT")
            {
                await ShowItalianNews(selected);
            }
        }
    }
}
