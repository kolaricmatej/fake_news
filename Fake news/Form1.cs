﻿using Fake_news.Forms;
using FontAwesome.Sharp;
using NewsAPI;
using NewsAPI.Constants;
using NewsAPI.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Fake_news
{
    public partial class Form1 : Form
    {
        private IconButton currentBtn;
        private Panel leftBorderBtn;
        private Form currentChildForm;

        private NewsApiClient apiClient;
        private ArticlesResult articleResponse;
        private string[] category;
        public Form1()
        {
            InitializeComponent();
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 60);
            panelMenu.Controls.Add(leftBorderBtn);
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;

            try
            {
                apiClient = new NewsApiClient("cebd043869544620b7b47b8b03846a12");
            }
            catch (Exception e)
            {
                apiClient = new NewsApiClient("6b4bbc08acee4f449da6d26b46bd6c13");
            }
            string[] language = new string[] { "Select", "EN", "HR", "DE", "IT" };
            comboBoxLanguage.DataSource = language;
        }

        
        private struct RGBColors
        {
            public static Color color1 = Color.FromArgb(172, 126, 241);
            public static Color color2 = Color.FromArgb(249, 118, 176);
            public static Color color3 = Color.FromArgb(253, 138, 114);
        }

        private void ActiveButton(object senderBtn,Color color)
        {
            if (senderBtn != null)
            {
                DisableButton();

                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(152, 31, 49);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = Color.White;
                currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;

                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0, currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();

                iconCurrent.IconChar = currentBtn.IconChar;
                iconCurrent.IconColor= Color.FromArgb(152, 31, 49);
                
            }
        }

        private void DisableButton()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(36, 36, 36);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.FromArgb(152, 31, 49);
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        private void OpenChildForm(Form childForm)
        {
            currentChildForm?.Close();
            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelDesktop.Controls.Add(childForm);
            panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            lblTitleChildForm.Text = currentBtn.Text;

        }

        private void iconButtonSearch_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.color1);
            OpenChildForm(new FormCheck());
        }

        private void iconButtonFind_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.color2);
            OpenChildForm(new ShowNews());
        }

        private void iconButtonAbout_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.color3);
            OpenChildForm(new About());
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            Reset();
           
        }

        private void Reset()
        {
            DisableButton();
            leftBorderBtn.Visible = false;
            iconCurrent.IconChar = IconChar.Home;
            iconCurrent.IconColor= Color.FromArgb(152, 31, 49);
            lblTitleChildForm.Text = "Home";
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panelTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void iconExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void iconMaximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }

        }

        private void iconMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// /NEWS
        /// </summary>
        /// <returns></returns>
        private string ChangeApi()
        {

            if (articleResponse.Status == Statuses.Error)
            {
                return "6b4bbc08acee4f449da6d26b46bd6c13";
            }
            else
            {
                return "cebd043869544620b7b47b8b03846a12";
            }
        }
        private async void ArrangeCategory()
        {
            if (comboBoxLanguage.SelectedItem == "EN" || comboBoxLanguage.SelectedItem == "Select")
            {
                category = new string[] { "News", "Sport", "Health", "Technology" };
            }
            else if (comboBoxLanguage.SelectedItem == "HR" )
            {
                category = new string[] { "Vijesti", "Sport", "Zdravlje", "Tehnologija" };
            }else if (comboBoxLanguage.SelectedItem == "DE")
            {
                category=new string[]{ "Nachrichten", "Sport", "Gesundheit", "Technologie" };
            }else if (comboBoxLanguage.Text == "IT")
            {
                category=new string[]{ "Notizie", "Sport", "Salute", "Tecnologia" };
            }
            comboBoxCategory.DataSource = category;
        }
        private void getDataFromNews()
        {
         flowLayoutPanel1.Controls.Clear();   
            if (articleResponse != null)
            {
                ListItem[] list = new ListItem[articleResponse.Articles.Count];
                for (int i = 0; i < 1; i++)
                {
                    foreach (var article in articleResponse.Articles)
                    {
                        list[i] = new ListItem();
                        list[i].Title = article.Title;
                        list[i].Published = (DateTime)article.PublishedAt;
                        list[i].Source = article.Source.Name;
                        list[i].Description = article.Description;
                        list[i].Url = article.Url;
                        try
                        {
                            WebClient wc = new WebClient();
                            byte[] bytes = wc.DownloadData(article.UrlToImage);
                            MemoryStream ms = new MemoryStream(bytes);
                            list[i].Image = System.Drawing.Image.FromStream(ms);
                        }
                        catch (Exception e)
                        {

                        }
                        flowLayoutPanel1.Controls.Add(list[i]);
                    }
                }
            }
        }

        private int ShowYear()
        {
            if (DateTime.Now.Month == 1)
            {
                return DateTime.Now.Year - 1;
            }
            else
            {
                return DateTime.Now.Year;
            }
        }
        private int ShowMonth()
        {
            if (DateTime.Now.Day < 10)
            {
                return DateTime.Now.Month - 1;
            }
            else
            {
                return DateTime.Now.Month;
            }
        }
        private int ShowDay()
        {
            if (DateTime.Now.Day < 10)
            {
                return DateTime.DaysInMonth(ShowYear(),ShowMonth())  - (10 - DateTime.Now.Day);
            }
            return DateTime.Now.Day - 10;
        }

        private async Task ShowCroatianNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.HR,
                PageSize = 50,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async Task ShowEnglishNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.EN,
                Page=3,
                PageSize = 20,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async Task ShowGermanNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.DE,
                PageSize = 50,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async Task ShowItalianNews(string category)
        {
            articleResponse = await apiClient.GetEverythingAsync(new EverythingRequest
            {
                Q = category,
                SortBy = SortBys.Popularity,
                Language = Languages.IT,
                PageSize = 50,
                From = new DateTime(ShowYear(), ShowMonth(), ShowDay())
            });
            if (articleResponse.Status == Statuses.Ok)
            {
                txtNumber.Text = articleResponse.TotalResults.ToString();
                getDataFromNews();
            }
        }

        private async void comboBoxLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = (string)comboBoxLanguage.SelectedItem;
           
            if (selected == "HR")
            {
                 ArrangeCategory();
            }
            else if (selected == "Select" || selected == "EN")
            {
                 ArrangeCategory();
            }else if (selected == "DE")
            {
                 ArrangeCategory();
            }else if (selected == "IT")
            {
                 ArrangeCategory();
            }
        }

        private async void comboBoxCategory_SelectedIndexChangedAsync(object sender, EventArgs e)
        {
            string selected = (string)comboBoxCategory.SelectedItem;

            if (comboBoxLanguage.Text == "HR" )
            {
                await ShowCroatianNews(selected);
            }else if (comboBoxLanguage.Text == "EN" || comboBoxLanguage.Text == "Select")
            {
                 await ShowEnglishNews(selected);
            }else if (comboBoxLanguage.Text == "DE")
            {
                 await ShowGermanNews(selected);
            }else if (comboBoxLanguage.Text == "IT")
            {
                 await ShowItalianNews(selected);
            }
        }


        private async void Form1_Load(object sender, EventArgs e)
        {
            await ShowEnglishNews("Technology");
        }
    }
}
