﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fake_newsML.Model;

using System.Runtime.InteropServices;
using System.Management;

namespace Fake_news.Forms
{
    public partial class FormCheck : Form
    {
        public FormCheck()
        {
            InitializeComponent();
            string[]category= new string[]{"Select","Trump", "Hillary","Trump & Hillary", "Other"};
            comboBoxCategory.DataSource = category;
        }

        public string GetTitle(string uri)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var str = reader.ReadToEnd();
                    Regex reg = new Regex("<title>(.*)</title>");
                    MatchCollection m = reg.Matches(str);
                    if (m.Count > 0)
                    {
                        return m[0].Value.Replace("<title>", "").Replace("</title>", "");
                    }
                    else
                        return "";
                }
            }
            catch (Exception e)
            {
                return labelError.Text = "You should insert correct URL\n";
            }

        }

        public float CalculatePercentage(double value)
        {
            float percentage= 100 * (1.0f / (1.0f + (float)Math.Exp(-value)));
            return percentage;
        }
        private void iconButtonSearch_Click(object sender, EventArgs e)
        {
            lblPrediction.Text = "";
            labelError.Text = "";
            progressBarProbability.Value = 0;
            string title = "";
            if (txtURL.Text.Contains("http://") || txtURL.Text.Contains("https://") || txtURL.Text.Contains("www."))
            {
                 title = GetTitle(txtURL.Text);
            }
            else
            {
                title = txtURL.Text;
            }
            var input = new ModelInput();
            input.Title = title;
            input.Content = txtStatement.Text;
            input.Publication = txtPublication.Text;
            string category = comboBoxCategory.Text;
            switch (category)
            {
                case "Trump":
                    input.Category = "Trump";
                    break;
                case "Hillary":
                    input.Category = "Hillary";
                    break;
                case "Trump & Hillary":
                    input.Category = "T&H";
                    break;
                case "Other":
                    input.Category = "Other";
                    break;
                case "Select":
                    labelError.Text += "You should insert category\n";
                    break;
                default:
                    break;
            }

            try
            {
                if (txtPublication.Text != "" && txtURL.Text != "" && comboBoxCategory.Text != "Select")
                {
                    ModelOutput result = ConsumeModel.Predict(input);
                    
                    if (result.Prediction == "0")
                    {
                        lblPrediction.Text = "This news are probably FAKE";
                        lblScore.Visible = true;
                        progressBarProbability.Visible = true;
                        iconButtonTurnIn.Visible = true;
                        progressBarProbability.Value = (int)(result.Score[0]*100);
                        progressBarProbability.SetState(2);
                    }
                    else
                    {
                        lblPrediction.Text = "This news are probably REAL";
                        lblScore.Visible = true;
                        progressBarProbability.Visible = true;
                        iconButtonTurnIn.Visible = true;
                        progressBarProbability.SetState(1);
                        progressBarProbability.Value = (int)(result.Score[1]*100);
                    }
                }
            }
            catch (Exception exception)
            {
                labelError.Text += "You have to fill out all fields\n";
                throw exception;
            }
        }

        private void FormCheck_Load(object sender, EventArgs e)
        {
            lblScore.Visible = false;
            progressBarProbability.Visible = false;
            iconButtonTurnIn.Visible = false;
        }

        private void iconButtonTurnIn_Click(object sender, EventArgs e)
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIPs)
                MessageBox.Show(address.ToString());
        }
    }
    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }

}
