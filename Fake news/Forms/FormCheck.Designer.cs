﻿namespace Fake_news.Forms
{
    partial class FormCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStatement = new System.Windows.Forms.Label();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.lblPublication = new System.Windows.Forms.Label();
            this.txtPublication = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.iconButtonSearch = new FontAwesome.Sharp.IconButton();
            this.progressBarProbability = new System.Windows.Forms.ProgressBar();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblPrediction = new System.Windows.Forms.Label();
            this.iconButtonTurnIn = new FontAwesome.Sharp.IconButton();
            this.labelURL = new System.Windows.Forms.Label();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.labelError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblStatement
            // 
            this.lblStatement.AutoSize = true;
            this.lblStatement.Location = new System.Drawing.Point(13, 84);
            this.lblStatement.Name = "lblStatement";
            this.lblStatement.Size = new System.Drawing.Size(262, 17);
            this.lblStatement.TabIndex = 0;
            this.lblStatement.Text = "Enter a doubtful sentance or paragraph:";
            // 
            // txtStatement
            // 
            this.txtStatement.Location = new System.Drawing.Point(16, 103);
            this.txtStatement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStatement.Multiline = true;
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.Size = new System.Drawing.Size(603, 51);
            this.txtStatement.TabIndex = 1;
            // 
            // lblPublication
            // 
            this.lblPublication.AutoSize = true;
            this.lblPublication.Location = new System.Drawing.Point(12, 166);
            this.lblPublication.Name = "lblPublication";
            this.lblPublication.Size = new System.Drawing.Size(118, 17);
            this.lblPublication.TabIndex = 2;
            this.lblPublication.Text = "Enter publication:";
            // 
            // txtPublication
            // 
            this.txtPublication.Location = new System.Drawing.Point(136, 166);
            this.txtPublication.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPublication.Multiline = true;
            this.txtPublication.Name = "txtPublication";
            this.txtPublication.Size = new System.Drawing.Size(304, 22);
            this.txtPublication.TabIndex = 3;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(13, 201);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(127, 17);
            this.lblCategory.TabIndex = 4;
            this.lblCategory.Text = "Choose cathegory:";
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(146, 201);
            this.comboBoxCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(161, 24);
            this.comboBoxCategory.TabIndex = 6;
            // 
            // iconButtonSearch
            // 
            this.iconButtonSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iconButtonSearch.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButtonSearch.IconChar = FontAwesome.Sharp.IconChar.Search;
            this.iconButtonSearch.IconColor = System.Drawing.Color.DarkRed;
            this.iconButtonSearch.IconSize = 16;
            this.iconButtonSearch.Location = new System.Drawing.Point(327, 201);
            this.iconButtonSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.iconButtonSearch.Name = "iconButtonSearch";
            this.iconButtonSearch.Rotation = 0D;
            this.iconButtonSearch.Size = new System.Drawing.Size(99, 28);
            this.iconButtonSearch.TabIndex = 7;
            this.iconButtonSearch.Text = "Search";
            this.iconButtonSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.iconButtonSearch.UseVisualStyleBackColor = true;
            this.iconButtonSearch.Click += new System.EventHandler(this.iconButtonSearch_Click);
            // 
            // progressBarProbability
            // 
            this.progressBarProbability.Location = new System.Drawing.Point(144, 265);
            this.progressBarProbability.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBarProbability.Name = "progressBarProbability";
            this.progressBarProbability.Size = new System.Drawing.Size(180, 25);
            this.progressBarProbability.TabIndex = 8;
            // 
            // lblScore
            // 
            this.lblScore.AutoEllipsis = true;
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(91, 265);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(49, 17);
            this.lblScore.TabIndex = 9;
            this.lblScore.Text = "Score:";
            // 
            // lblPrediction
            // 
            this.lblPrediction.AutoSize = true;
            this.lblPrediction.Location = new System.Drawing.Point(234, 327);
            this.lblPrediction.Name = "lblPrediction";
            this.lblPrediction.Size = new System.Drawing.Size(0, 17);
            this.lblPrediction.TabIndex = 10;
            // 
            // iconButtonTurnIn
            // 
            this.iconButtonTurnIn.BackColor = System.Drawing.Color.Firebrick;
            this.iconButtonTurnIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButtonTurnIn.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButtonTurnIn.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.iconButtonTurnIn.IconChar = FontAwesome.Sharp.IconChar.None;
            this.iconButtonTurnIn.IconColor = System.Drawing.Color.Black;
            this.iconButtonTurnIn.IconSize = 16;
            this.iconButtonTurnIn.Location = new System.Drawing.Point(348, 265);
            this.iconButtonTurnIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.iconButtonTurnIn.Name = "iconButtonTurnIn";
            this.iconButtonTurnIn.Rotation = 0D;
            this.iconButtonTurnIn.Size = new System.Drawing.Size(114, 25);
            this.iconButtonTurnIn.TabIndex = 11;
            this.iconButtonTurnIn.Text = "TURN IN";
            this.iconButtonTurnIn.UseVisualStyleBackColor = false;
            this.iconButtonTurnIn.Click += new System.EventHandler(this.iconButtonTurnIn_Click);
            // 
            // labelURL
            // 
            this.labelURL.AutoSize = true;
            this.labelURL.Location = new System.Drawing.Point(13, 35);
            this.labelURL.Name = "labelURL";
            this.labelURL.Size = new System.Drawing.Size(208, 17);
            this.labelURL.TabIndex = 13;
            this.labelURL.Text = "Enter URL or Title of the article:";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(16, 54);
            this.txtURL.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(603, 22);
            this.txtURL.TabIndex = 15;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.ForeColor = System.Drawing.Color.DarkRed;
            this.labelError.Location = new System.Drawing.Point(234, 302);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(0, 17);
            this.labelError.TabIndex = 16;
            // 
            // FormCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.labelURL);
            this.Controls.Add(this.iconButtonTurnIn);
            this.Controls.Add(this.lblPrediction);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.progressBarProbability);
            this.Controls.Add(this.iconButtonSearch);
            this.Controls.Add(this.comboBoxCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.txtPublication);
            this.Controls.Add(this.lblPublication);
            this.Controls.Add(this.txtStatement);
            this.Controls.Add(this.lblStatement);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormCheck";
            this.Text = "FormCheck";
            this.Load += new System.EventHandler(this.FormCheck_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStatement;
        private System.Windows.Forms.TextBox txtStatement;
        private System.Windows.Forms.Label lblPublication;
        private System.Windows.Forms.TextBox txtPublication;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox comboBoxCategory;
        private FontAwesome.Sharp.IconButton iconButtonSearch;
        private System.Windows.Forms.ProgressBar progressBarProbability;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblPrediction;
        private FontAwesome.Sharp.IconButton iconButtonTurnIn;
        private System.Windows.Forms.Label labelURL;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label labelError;
    }
}